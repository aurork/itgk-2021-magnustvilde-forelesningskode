'''betingelser'''
a = 5
b = 3
c = 5

# test ut <, >, <=, >=, ==, !=
mindre =  (a<b)
storre = (a>b)
mindreLik = (a <= b)
storreLik = a>=c
lik = a == c
ulik = a != c


print(f'Mindre enn: {mindre}')
print(f'Større enn: {storre}')
print(f'Er mindre enn, eller lik: {mindreLik}')
print(f'Er større enn, eller lik: {storreLik}')
print(f'Er helt lik: {lik}')
print(f'Er ulik: {ulik}')

